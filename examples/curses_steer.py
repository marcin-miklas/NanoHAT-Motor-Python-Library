#!/usr/bin/python
from curses import wrapper, error

from FriendlyELEC_NanoHatMotor import FriendlyELEC_NanoHatMotor as NanoHatMotor
import bakebit
from threading import Thread, Event
import time

class Motor(object):
    controller = NanoHatMotor(addr=0x60)

    def __init__(self, number):
        self.motor = Motor.controller.getMotor(number)
        self._speed = 0

    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self, new_speed):
        new_speed = max(-255, min(new_speed, 255))
        if new_speed == self._speed:
            return
        if new_speed > 0:
            self.motor.setSpeed(new_speed)
            if not self._speed > 0:
                self.motor.run(NanoHatMotor.FORWARD)
        elif new_speed < 0:
            self.motor.setSpeed(-new_speed)
            if not self._speed < 0:
                self.motor.run(NanoHatMotor.BACKWARD)
        else:  # new_speed == 0
            if not self._speed == 0:
                self.motor.run(NanoHatMotor.RELEASE)
        self._speed = new_speed


class Ultrasonic(Thread):
    ultrasonic_ranger = 4

    def __init__(self, motors):
        Thread.__init__(self)
        self.motors = motors
        self.distance = 0
        self.running = Event()
        self.running.set()
        
    def run(self):
        while self.running.is_set():
            try:
                self.distance = bakebit.ultrasonicRead(self.ultrasonic_ranger)
                if 0 < self.distance < 10:
                    for m in self.motors:
                        m.speed = 0
                time.sleep(.2)
            except (TypeError, IOError) as err:
                pass

    def stop(self):
        self.running.clear()


def main(win):
    try:
        left = Motor(4)
        right = Motor(1)
        buzzer = 5
        led = 3
        bakebit.pinMode(led, "OUTPUT")
        ultrasonic = Ultrasonic((left, right))
        ultrasonic.start()
        buzz = 0
        light = 0

        win.timeout(300)

        while True:
            try:
                win.addstr(0, 0, "Speed %4d %4d, Distance %4d  " % (left.speed, right.speed, ultrasonic.distance))
                key = win.getkey()
            except error:
                left.speed /= 2
                right.speed /= 2
                if buzz:
                    buzz = 0
                    bakebit.analogWrite(buzzer, 0)
                if light:
                    light = 0
                    bakebit.analogWrite(led, 0)
                continue
            if key == 'KEY_UP':
                left.speed += 8
                right.speed += 8
            elif key == 'KEY_DOWN':
                left.speed -= 8
                right.speed -= 8
            elif key == 'KEY_LEFT':
                left.speed -= 8
                right.speed += 8
            elif key == 'KEY_RIGHT':
                left.speed += 8
                right.speed -= 8
            elif key == ' ':
                light = 1
                bakebit.digitalWrite(led, 1)
                left.speed = 0
                right.speed = 0
            elif key == 'r':
                left.speed = -left.speed
                right.speed = -right.speed
            elif key == 'a':
                left.speed -= 64
                right.speed += 64
            elif key == 'd':
                left.speed += 64
                right.speed -= 64
            elif key == 'w':
                left.speed += 64
                right.speed += 64
            elif key == 's':
                left.speed -= 64
                right.speed -= 64
            elif key == 'z':
                buzz = 1
                bakebit.analogWrite(buzzer, 127)

    finally:
        ultrasonic.stop()
        Motor.controller.getMotor(1).run(NanoHatMotor.RELEASE)
        Motor.controller.getMotor(2).run(NanoHatMotor.RELEASE)
        Motor.controller.getMotor(3).run(NanoHatMotor.RELEASE)
        Motor.controller.getMotor(4).run(NanoHatMotor.RELEASE)


wrapper(main)
