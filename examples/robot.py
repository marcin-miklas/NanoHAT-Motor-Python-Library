#!/usr/bin/python
from curses import wrapper

from FriendlyELEC_NanoHatMotor import FriendlyELEC_NanoHatMotor as NanoHatMotor


class Motor(object):
    controller = NanoHatMotor(addr=0x60)

    def __init__(self, number):
        self.motor = Motor.controller.getMotor(number)
        self._speed = 0

    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self, new_speed):
        new_speed = max(-255, min(new_speed, 255))
        if new_speed == self._speed:
            return
        if new_speed > 0:
            self.motor.setSpeed(new_speed)
            if not self._speed > 0:
                self.motor.run(NanoHatMotor.FORWARD)
        elif new_speed < 0:
            self.motor.setSpeed(-new_speed)
            if not self._speed < 0:
                self.motor.run(NanoHatMotor.BACKWARD)
        else:  # new_speed == 0
            if not self._speed == 0:
                self.motor.run(NanoHatMotor.RELEASE)
        self._speed = new_speed


def main(win):
    try:
        left = Motor(1)
        right = Motor(2)

        while True:
            win.addstr(0, 0, "Speed %4d %4d" % (left.speed, right.speed))
            key = win.getkey()
            if key == 'KEY_UP':
                left.speed += 8
                right.speed += 8
            elif key == 'KEY_DOWN':
                left.speed -= 8
                right.speed -= 8
            elif key == 'KEY_LEFT':
                left.speed -= 8
                right.speed += 8
            elif key == 'KEY_RIGHT':
                left.speed += 8
                right.speed -= 8
            elif key == ' ':
                left.speed = 0
                right.speed = 0
            elif key == 'r':
                left.speed = -left.speed
                right.speed = -right.speed
            elif key == 'a':
                left.speed -= 64
                right.speed += 64
            elif key == 'd':
                left.speed += 64
                right.speed -= 64
            elif key == 'w':
                left.speed += 64
                right.speed += 64
            elif key == 's':
                left.speed -= 64
                right.speed -= 64
    finally:
        Motor.controller.getMotor(1).run(NanoHatMotor.RELEASE)
        Motor.controller.getMotor(2).run(NanoHatMotor.RELEASE)
        Motor.controller.getMotor(3).run(NanoHatMotor.RELEASE)
        Motor.controller.getMotor(4).run(NanoHatMotor.RELEASE)


wrapper(main)
